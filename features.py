
# Select training features here:

JET_NUMERICAL = ['log_pt', 'eta', 'mass', 'phi', 'area', 'qgl_axis2', 'qgl_ptD', 'qgl_mult']
PF_NUMERICAL = ['rel_pt', 'rel_eta', 'rel_phi', 'd0', 'dz', 'd0Err', 'dzErr', 'trkChi2', 'vtxChi2', 'puppiWeight', 'puppiWeightNoLep']

CATEGORICAL_MAP = {
    'jet': {
        'partonFlavour': [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 21]
    },
    'pf': {
        'charge': [-1, 0, 1],
        'lostInnerHits': [-1, 0, 1, 2],
        'pdgId': [-211, -13, -11, 1, 2, 11, 13, 22, 130, 211],
        'pvAssocQuality': [0, 1, 4, 5, 6, 7],
        'trkQuality': [0, 1, 5]
    }
}


def get_features():
    jet_categorical = []
    for key, categories in CATEGORICAL_MAP['jet'].items():
        jet_categorical.extend([f'{key}_{i}' for i in range(len(categories))])
        
    pf_categorical = []
    for key, categories in CATEGORICAL_MAP['pf'].items():
        pf_categorical.extend([f'{key}_{i}' for i in range(len(categories))])

    jet_fields = JET_NUMERICAL + jet_categorical
    pf_fields = PF_NUMERICAL + pf_categorical

    jet_keys = [f'jet_{field}' for field in jet_fields]
    pf_keys = [f'pf_{field}' for field in pf_fields]

    num_jet = len(jet_keys)
    num_pf = len(pf_keys)

    class DotDict:
        pass
    
    fts = DotDict()

    fts.jet_fields = jet_fields
    fts.pf_fields = pf_fields
    fts.jet_keys = jet_keys
    fts.pf_keys = pf_keys
    fts.num_jet = num_jet
    fts.num_pf = num_pf

    return fts