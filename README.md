# jec-tfjob

Run jec-dnn as a distributed training job using TFJob CRD and Katib UI.  

### How to run
- Build docker image with custom code: `docker build -f Dockerfile -t gitlab-registry.cern.ch/dholmber/jec-tfjob:<VERSION> . --network=host`
- Push docker image: `docker push gitlab-registry.cern.ch/dholmber/jec-tfjob:<VERSION>`
- Edit custom-code.yaml
    - Select number of workers
    - Select your docker image `image: gitlab-registry.cern.ch/dholmber/jec-tfjob:<VERSION>`
- Go to https://ml.cern.ch/katib/#/katib/hp, and copy paste content of custom-code.yaml
- Click _Deploy_
