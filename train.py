import tensorflow as tf
import argparse
from data import get_datasets
from particle_net import get_particle_net


def get_callbacks():
    # Reduce learning rate when nearing convergence
    reduce_lr_on_plateau = tf.keras.callbacks.ReduceLROnPlateau(
        monitor='val_loss', factor=0.2, patience=5, min_lr=1.0e-8,
        mode='auto', min_delta=1.0e-4, cooldown=0, verbose=1
    )
    # Stop early if the network stops improving
    early_stopping = tf.keras.callbacks.EarlyStopping(
        monitor='val_loss', min_delta=1.0e-4, 
        patience=7, mode='auto', baseline=None, 
        restore_best_weights=True, verbose=1
    )

    return [reduce_lr_on_plateau, early_stopping]


if __name__ == '__main__':
    print('Cheers!')
    arg_parser = argparse.ArgumentParser(description=__doc__)
    arg_parser.add_argument('--data_dir', type=str, required=True, help='Dataset directory')
    # arg_parser.add_argument('--results_dir', type=str, required=True, help='Results directory')
    arg_parser.add_argument('--epochs', type=int, default=5, help='Number of epochs')
    arg_parser.add_argument('--batch_size', type=int, default=512, help='Batch size')
    arg_parser.add_argument('--train_size', type=float, default=0.6, help='Training set size')
    arg_parser.add_argument('--val_size', type=float, default=0.2, help='Validation set size')
    arg_parser.add_argument('--test_size', type=float, default=0.2, help='Test set size')
    arg_parser.add_argument('--shuffle_buffer', type=int, default=100, help='Shuffle buffer for training set')
    arg_parser.add_argument('--learning_rate', type=float, default=0.001, help='Initial learning rate')
    arg_parser.add_argument('--loss', type=str, default='mean_absolute_error', help='Loss function')
    arg_parser.add_argument('--optimizer', type=str, default='adam', help='Optimizer')
    arg_parser.add_argument('--activation', type=str, default='relu', help='Activation function')
    arg_parser.add_argument('--initializer', type=str, default='he_normal', help='Weight initializer')
    arg_parser.add_argument('--pooling', type=str, default='average', help='Conv pooling')
    arg_parser.add_argument('--batch_norm', type=int, default=0, help='With batch norm')
    arg_parser.add_argument('--shortcut', type=int, default=0, help='With shortcut')
    arg_parser.add_argument('--dropout', type=float, default=0.0, help='Dropout rate')
    arg_parser.add_argument('--num_points', type=int, default=100, help='Constituent cutoff for ParticleNet')
    arg_parser.add_argument('--k', type=int, default=0, help='Used py ParticleNet for k-NN')
    
    args = arg_parser.parse_args()

    train_ds, val_ds, test_ds = get_datasets(args)

    strategy = tf.distribute.MirroredStrategy()
    with strategy.scope():
        dnn = get_particle_net(args)
        dnn.compile(optimizer=args.optimizer, loss=args.loss)
        dnn.optimizer.lr.assign(args.learning_rate)

    # tf.keras.utils.plot_model(dnn, dpi=100, show_shapes=True, expand_nested=True)

    fit = dnn.fit(train_ds, validation_data=val_ds, epochs=args.epochs) #, callbacks=get_callbacks())

    result = dnn.evaluate(test_ds)
    print(result)