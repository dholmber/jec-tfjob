import tensorflow as tf
import awkward as ak
import numpy as np
from glob import glob
import os
from features import get_features


def get_datasets(args):
    dirs = glob(os.path.join(args.data_dir, '*'))
    num_dirs = len(dirs)
    train_split = int(args.train_size * num_dirs)
    test_split = int(args.test_size * num_dirs) + train_split

    train_dirs = dirs[:train_split]
    test_dirs = dirs[train_split:test_split]
    val_dirs = dirs[test_split:]

    fts = get_features()

    train_ds = create_dataset(train_dirs, args, fts).shuffle(args.shuffle_buffer)
    val_ds = create_dataset(val_dirs, args, fts)
    test_ds = create_dataset(test_dirs, args, fts)

    return train_ds, val_ds, test_ds


def create_dataset(paths, args, fts):
    ds = tf.data.Dataset.from_tensor_slices(paths)
    ds = ds.map(lambda path: read_parquet_wrapper(path, args, fts), num_parallel_calls=tf.data.experimental.AUTOTUNE)
    ds = ds.map(lambda data: prepare_pf_inputs(data, fts), num_parallel_calls=tf.data.experimental.AUTOTUNE)
    ds = ds.unbatch().batch(args.batch_size)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    return ds


def prepare_pf_inputs(data, fts):
    data['mask'] = tf.cast(tf.math.not_equal(data['pf_rel_eta'], 0), dtype=tf.float32) # 1 if valid
    data['coord_shift'] = tf.multiply(1e6, tf.cast(tf.math.equal(data['mask'], 0), dtype=tf.float32))
    data['points'] = tf.concat([data['pf_rel_eta'], data['pf_rel_phi']], axis=2)
    
    jet_data = tf.concat([data[key] for key in fts.jet_keys], axis=1)
    pf_data = tf.concat([data[key] for key in fts.pf_keys], axis=2)
    
    inputs = (pf_data, jet_data, data['points'], data['coord_shift'], data['mask'])
    return inputs, data['target']


def read_parquet_wrapper(path, args, fts):
    inp = [path, fts.jet_fields, fts.pf_fields, args.num_points]
    Tout = [tf.float32] + [tf.float32] * fts.num_jet + [tf.float32] * fts.num_pf
    
    cols = tf.numpy_function(read_parquet, inp=inp, Tout=Tout)
    
    keys = ['target'] + fts.jet_keys + fts.pf_keys
    data = {key: value for key, value in zip(keys, cols)}
    
    data['target'].set_shape((None,))
    
    for key in fts.jet_keys:
        # Shape from <unknown> to (None,)
        data[key].set_shape((None,))
        # Shape from (None,) to (None, 1)
        data[key] = tf.expand_dims(data[key], axis=1)
    
    for key in fts.pf_keys:
        # Shape from <unknown> to (None, P)
        data[key].set_shape((None, args.num_points))
        # Shape from (None, P) to (None, P, 1)
        data[key] = tf.expand_dims(data[key], axis=2)
    
    return data


def read_parquet(path, jet_fields, pf_fields, num_points):
    path = path.decode()

    jet = ak.from_parquet(os.path.join(path, 'jet.parquet'))
    pf = ak.from_parquet(os.path.join(path, 'pf.parquet'))
    
    data = [ak.to_numpy(jet.target).astype(np.float32)]
    
    for field in jet_fields:
        data.append(ak.to_numpy(jet[field]).astype(np.float32))

    for field in pf_fields:
        none_padded_pf = ak.pad_none(pf[field], target=num_points, clip=True, axis=1)
        zero_padded_pf = ak.to_numpy(none_padded_pf).filled(0)
        data.append(zero_padded_pf.astype(np.float32))
    
    return data
